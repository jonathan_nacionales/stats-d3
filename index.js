var d3 = require("d3");

function ObjectivesView( objectives, facade ) {
   facade.addCallback( 
      "OBJECTIVES_UPDATED",
      objectivesUpdatedHandler );

	function objectivesUpdatedHandler( data ) {
		console.log( data );

		var items;

		var maxBlue = 2;
		var maxRed = 2;

		items = restructureObjectiveData( data.data );
		items.blue.sort( function(a, b) {
			return b.time <= a.time;
		});
		items.red.sort( function(a, b) {
			return b.time <= a.time;
		});

		var blueItems = items.blue.filter( new filterItems({}) );
		var redItems = items.red.filter( new filterItems({}) );

		var items = blueItems.concat( redItems );


		function filterItems( ledger ) {
			return function( item ) {
				if( !ledger[ item.time ] ) 
					ledger[ item.time ] = 0;

				item.position = ledger[ item.time];

				ledger[ item.time ]++;

				return item;
			}
			
		}

		var padding = (maxBlue + maxRed)*20;
		var topPadding = maxBlue*20;
		var bottomPadding = maxRed*20;

		var size = data.time_length;
		size = (Math.floor(size / 5.0) + 1) * 5;

		var trueWidth = 750;
		var trueHeight = (750*80)/570;
		var delta = 30;

		var margin = {top: 30, right: 20, bottom: 30, left: 50},
			 width = trueWidth - margin.left - margin.right,
			 height = (trueHeight + maxBlue*20 + maxRed*20) 
				- margin.top 
				- margin.bottom;

		var x = d3.scale.linear()
		 .domain([0, size])
		 .range([0, width]);

		var imageScale = d3.scale.linear()
			.domain([0, 180])
			.range([20, 0]);

		d3.select(objectives).select("svg").remove();

		var svg = d3.select(objectives).append("svg")
			 .attr("width", width + margin.left + margin.right)
			 .attr("height", height + margin.top + margin.bottom);

		var objectiveTop = svg.append("g")
			.attr("id", "objective_top_axis")
			.attr("class", "objective_top_axis")
			.attr("transform", function() {
				return "translate(" + margin.left + "," + (margin.top + topPadding) + ")";
			});

		objectiveTop
			.call( 
				d3.svg.axis()
					.scale(x)
					.orient("bottom")
					.ticks(size/5, "s"));

		var topMinorTicks = objectiveTop.selectAll("line")
			.data( x.ticks(size), function(d) {
				return d;
			})
			.enter()
			.append("line")
			.attr("class", "minor_ticks")
			.call( scaleTopMinorTicks );

		var objectiveBottom = svg.append("g")
			.attr("id", "objective_bottom_axis")
			.attr("class", "objective_bottom_axis")
			.attr("transform", function() {
				var padding = maxBlue + maxRed;

				return "translate(" + margin.left + "," + (27 + margin.top + topPadding) + ")";
			});


		objectiveBottom
			.call( 
				d3.svg.axis()
					.scale(x)
					.orient("top")
					.ticks(size/5, "s"));

		var bottomMinorTicks = objectiveBottom.selectAll("line")
			.data( x.ticks(size), function(d) {
				return d;
			})
			.enter()
			.append("line")
			.attr("class", "minor_ticks")
			.call( scaleBottomMinorTicks );

		var images = svg.append("g")
			.attr("class", "image")
			.selectAll(".image")
			.data( items )
			.enter()
			.append("svg:image")
			.attr("xlink:href", function(obj) {
			  var strategy = getImageStrategy(obj);
			  return strategy.getImage();
			})
			.call( position );

		function restructureObjectiveData( data ) {

			var ledger = {
			};

			data.forEach( function( item ) {
				ledger = getNewLedger( ledger, item );
			});

			var redData = [];
			var blueData = [];

			for( time in ledger[100] ) {
				var items = getNewItems( 100, time, ledger[100][time] );
				blueData = blueData.concat( items );
			}

			for( time in ledger[200] ) {
				var items = getNewItems( 200, time, ledger[200][time] );
				redData = redData.concat( items );
			}

			return {
				blue: blueData,
				red: redData
			}

			function getNewItems( team, time, events ) {
				var newEvents = [];

				for( event in events ) {
					var count = events[event];

					var item = {
						team: team,
						time: time,
						event: event,
						count: count
					};

					newEvents.push( item );
				}

				return newEvents;
			}

			function getNewLedger( ledger, item ) {
				var event = item.event;
				var time = item.time;
				var team = item.team;

				if( !ledger[team] )
					ledger[team] = {};

				if( !ledger[team][time] )
					ledger[team][time] = {};

				if( !ledger[team][time][event] )
					ledger[team][time][event] = 0;

				ledger[team][time][event] = ledger[team][time][event] + 1;

				return ledger;
			}

			function clone( o ) {
				return JSON.parse( JSON.stringify( o ) );
			}
		}

		function position( obj ) { 
			obj
				.attr("width", function(obj) {
					console.log( imageScale(size) );
					return imageScale(size);

				})
				.attr("height", function(obj) {
					return imageScale(size);
				})
				.attr( "x", function(obj) {
					return x(obj.time) + margin.left - imageScale(size)/2; 
				})
				.attr( "y", function(obj) {
					if( obj.team === 100 ) {
						return (imageScale(size)/2) 
							- (obj.position)*(imageScale(size)) 
							+ topPadding;
					}

					return 60 + obj.position*20 + topPadding;
				});
		}

		function scaleTopMinorTicks( obj ) {
			obj
				.attr("y1", 0)
				.attr("y2", 5)
				.attr("x1", function(d){
					return x(d);
					return xScale(d);

				})
				.attr("x2", function(d) {
					return x(d);
					return xScale(d);
				});
		}

		function scaleBottomMinorTicks( obj ) {
			obj
				.attr("y1", 0)
				.attr("y2", -5)
				.attr("x1", function(d){
					return x(d);
					return xScale(d);

				})
				.attr("x2", function(d) {
					return x(d);
					return xScale(d);
				});
		}

	//   svg.on("mousemove", function() {
	//      var mouse = d3.mouse(this);
	//      console.log(mouse);
	//
	//      xScale.distortion(2.5).focus(mouse[0]);
	//      newScale.distortion(2.5).focus(mouse[0]);
	//
	//      redraw();
	//   });

		function redraw() {

			images.call( position );
			topMinorTicks.call( scaleTopMinorTicks );
			bottomMinorTicks.call( scaleBottomMinorTicks );

			objectiveTop
				.call( 
					d3.svg.axis()
						.scale(xScale)
						.orient("bottom")
						.ticks(size/5, "s"));

			objectiveBottom
				.call( 
					d3.svg.axis()
						.scale(xScale)
						.orient("top")
						.ticks(size/5, "s"));
		}

		function getImageStrategy( obj ) {
			var strategies = [
				new BlueTowerStrategy(obj),
				new RedTowerStrategy(obj),
				new BlueInhibitorStrategy(obj),
				new RedInhibitorStrategy(obj),
				new BlueDragonStrategy(obj),
				new RedDragonStrategy(obj),
				new BlueBaronStrategy(obj),
				new RedBaronStrategy(obj)
			];

			var strategy = strategies.filter( function(strategy) {
				return strategy.test();
			});

			return strategy[0];
		}

		function BlueTowerStrategy( obj ) {
			this.test = function() {
				return (
					(obj.event === "TOWER_BUILDING") &&
					(obj.team === 100 ));
			}

			this.getImage = function() {
				return "img/BlueIcon"+obj.count+".png";

				return "img/Tower_blue.png";
			}
		}

		function RedTowerStrategy( obj ) {
			this.test = function() {
				return (
					(obj.event === "TOWER_BUILDING") &&
					(obj.team === 200 ));
			}

			this.getImage = function() {
				return "img/RedIcon"+obj.count+".png";

				return "img/Tower_Red.png";
			}
		}

		function BlueInhibitorStrategy( obj ) {
			this.test = function() {
				return (
					(obj.event === "INHIBITOR_BUILDING") &&
					(obj.team === 100 ));
			}

			this.getImage = function() {
				return "img/BlueInhib"+obj.count+".png";
				return "img/Inhibitor_Blue.png";
			}
		}

		function RedInhibitorStrategy( obj ) {
			this.test = function() {
				return (
					(obj.event === "INHIBITOR_BUILDING") &&
					(obj.team === 200 ));
			}

			this.getImage = function() {
				return "img/RedInhib"+obj.count+".png";
				return "img/Inhibitor_Red.png";
			}
		}

		function BlueDragonStrategy( obj ) {
			this.test = function() {
				return (
					(obj.event === "DRAGON") &&
					(obj.team === 100 ));
			}

			this.getImage = function() {
         return "img/BlueDragon.png";
			
				return "img/Dragon_Blue.png";
			}
		}

		function RedDragonStrategy( obj ) {
			this.test = function() {
				return (
					(obj.event === "DRAGON") &&
					(obj.team === 200 ));
			}

			this.getImage = function() {
         return "img/RedDragon.png";
				return "img/Dragon_Red.png";
			}
		}

		function BlueBaronStrategy( obj ) {
			this.test = function() {
				return (
					(obj.event === "BARON_NASHOR") &&
					(obj.team === 100 ));
			}

			this.getImage = function() {
         return "img/BlueBaron.png";

				return "img/Baron_Blue.png";
			}
		}

		function RedBaronStrategy( obj ) {
			this.test = function() {
				return (
					(obj.event === "BARON_NASHOR") &&
					(obj.team === 200 ));
			}

			this.getImage = function() {
         return "img/RedBaron.png";
				return "img/Baron_Red.png";
			}
		}
	}
}

function TimelineView( timeline, facade ) {

   facade.addCallback( 
      "TIMELINE_UPDATED",
      timelineUpdatedHandler );

	function timelineUpdatedHandler(data) {

		function roundToThousand( num ) {
			if( data.state === "gpm" )
				return (Math.floor(num/1000.0)+1) * 1000;
			else if( data.state === "cspm" )
				return (Math.floor(num/10.0)+1) * 10;
			else if( data.state === "kapm" )
				return (Math.floor(num) + 1);
			else if( data.state === "wpm" )
				return (Math.floor(num) + 1);
			else if( data.state === "xpm" )
				return (Math.floor(num/1000.0)+1) * 1000;
			else if( data.state === "cs" )
				return (Math.floor(num/100.0)+1) * 100;
			else if( data.state === "gold" )
				return (Math.floor(num/10000.0)+1) * 10000;
			else if( data.state === "ka" )
				return (Math.floor(num/10.0)+1) * 10;
			else if( data.state === "ward" )
				return (Math.floor(num/10.0)+1) * 10;
			else if( data.state === "xp" )
				return (Math.floor(num/10000.0)+1) * 10000;

			return (Math.floor(num/1000.0)+1) * 1000;
		}


		function getYAxisName() {
			return data.state;
		}

		function getData( data ) {
			return {
				blue: data.blue_team[data.state],
				red: data.red_team[data.state]
			};
		}

		var new_data = getData(data);
		var blue_value = new_data.blue;
		var red_value = new_data.red;

		var blue_data = [];
		var red_data = [];

		var maxY = 0;

		blue_value.forEach(function(value,index) {
			var index = index*5;
			var value = value[index];

			if( value > maxY )
				maxY = value;

			var data = {
				time: index,
				value: value
			};

			blue_data.push( data );
		});

		red_value.forEach(function(value, index) {
			var index = index*5;
			var value = value[index];

			if( value > maxY )
				maxY = value;

			var data = {
				time: index,
				value: value
			};

			red_data.push( data );
		});

		maxY = roundToThousand( maxY );

		var stats = {
			red_value: {},
			blue_value: {}
		};

		var trueWidth = 750;
		var trueHeight = (750*300)/570;
		var delta = 0;

		var margin = {top: 20, right: 20, bottom: 30, left: 50},
			 width = trueWidth - margin.left - margin.right,
			 height = trueHeight - margin.top - margin.bottom;

		var size = (red_data.length) * 5;

		var x = d3.scale.linear()
			.domain([0, size])
			.range([0, width]);

		var y = d3.scale.linear()
			 .domain([0,maxY])
			 .range([height, 0]);

		var xAxis = d3.svg.axis()
			 .scale(x)
			 .orient("bottom");

		var yAxis = d3.svg.axis()
			 .scale(y)
			 .orient("left");

		var line = d3.svg.line()
			 .x(function(d) { 
					return x(d.time) + delta; 
			  })
			 .y(function(d) { 
					return y(d.value); 
			  });

		d3.select(timeline).select("svg").remove();

		var svg = d3.select(timeline).append("svg")
			 .attr("width", width + margin.left + margin.right)
			 .attr("height", height + margin.top + margin.bottom);

		var svg1 = svg
			 .append("g")
			 .attr("id", "svg1")
			 .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		var svg2 = svg
			 .append("g")
			 .attr("id", "svg2")
			 .attr("transform", "translate(" + margin.left + "," + margin.top + ")");



		// Red

		  red_data.forEach(function(d) {
			 var time = d.time;
			 var value = d.value;
			 stats.red_value[time] = value;
		  });

		var xaxisg = svg.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(" + margin.left + "," + (height + margin.top) + ")")
			.call( 
				d3.svg.axis()
					.scale(x)
					.orient("bottom")
					.ticks(size/5, "s"));

		xaxisg.selectAll("line")
			.data( x.ticks(size), function(d) {
				return d;
			})
			.enter()
			.append("line")
			.attr("class", "minor")
			.attr("y1", 0)
			.attr("y2", 5)
			.attr("x1", x)
			.attr("x2", x);


		  svg2.append("path")
				.datum(red_data)
				.attr("class", "redline")
				.attr("d", line);

			var circles = svg2.selectAll(".redcircle")
				.data(red_data)
			.enter()
			.append("circle")
			.attr("cx", function(d) {
				return x(d.time) + delta;
			 })
			.attr("cy", function(d) {
				return y(d.value);
			})
			.attr("r", 4)
			.attr("fill", "red");

		// Blue

		  blue_data.forEach(function(d) {
			 var time = d.time;
			 var value = d.value;
			 stats.blue_value[time] = value;
		  });

		  svg2.append("g")
				.attr("class", "y axis")
				.call(yAxis)
			 .append("text")
				.attr("transform", "rotate(-90)")
				.attr("y", 6)
				.attr("dy", ".71em")
				.style("text-anchor", "end")
				.text(getYAxisName());


		  svg2.append("path")
				.datum(blue_data)
				.attr("class", function(d) {
					return "line";
				})
				.attr("class", "line")
				.attr("d", line);

			var blue_circles = svg2.selectAll(".blue_circle")
			.data(blue_data);

			blue_circles
			.enter()
			.append("circle")
			.attr("cx", function(d) {
				return x(d.time) + delta;
			 })
			.attr("cy", function(d) {
				return y(d.value);
			})
			.attr("r", 4)
			.attr("fill", "steelblue");

		  svg2.append("path")
				.datum(red_data)
				.attr("class", "redline")
				.attr("d", line);

			var circles = svg2.selectAll("circle")
				.data(red_data);

			circles
			.enter()
			.append("circle")
			.attr("cx", function(d) {
				return x(d.time) + delta;
			 })
			.attr("cy", function(d) {
				return y(d.value);
			})
			.attr("r", 4)
			.attr("fill", "red");

		  svg2.append("path")
				.datum(blue_data)
				.attr("class", function(d) {
					return "line";
				})
				.attr("class", "line")
				.attr("d", line);

			var blue_circles = svg2.selectAll(".blue_circle")
			.data(blue_data);

			blue_circles
			.enter()
			.append("circle")
			.attr("cx", function(d) {
				return x(d.time) + delta;
			 })
			.attr("cy", function(d) {
				return y(d.value);
			})
			.attr("r", 4)
			.attr("fill", "steelblue");

		var mouseX = 0;

		var blueSelectedCircle1 = svg2
			.append("circle");
		var blueSelectedCircle2 = svg2
			.append("circle");
		var blueSelectedCircle3 = svg2
			.append("circle");

		var redSelectedCircle1 = svg2
			.append("circle");
		var redSelectedCircle2 = svg2
			.append("circle");
		var redSelectedCircle3 = svg2
			.append("circle");

		var blueSelectedLine = svg2.append("line");
		var redSelectedLine = svg2.append("line");

		var valueDiffLabel = svg2.append("text");

		var blackVerticalLine = svg2.append("line");

		var svgA = d3.select("svg");
		var previousTime = -1;

		var visualization = "value";

		var rectangle = svg2.append("rect");

		svgA.on('mousemove', function () {
			var size = blue_data.length;
			size = size;

			mouseX = d3.mouse(this)[0];         

			if( mouseX < margin.left )
				return;

			if( mouseX > (trueWidth - x(5)) )
				return;

			/**
			 * TODO: Fix this hack
			 */
			var time = 0;
			var num = ((trueWidth ) / size);
			time = ((mouseX - num) / num) + 1.1;
			time = Math.floor( time );
			time = 5*time;

			if( previousTime === time ) {
				return;
			}

			previousTime = time;

			var blue_value = stats.blue_value[time];
			var red_value = stats.red_value[time];

			blackVerticalLine
				.attr("x1", function(d) {
					return x(time) + delta;
				})
				.attr("x2", function(d) {
					return x(time) + delta;
				})
				.attr("y1", function(d) {
					return height;
					return 350;
					return y(blue_value);
				})
				.attr("y2", function(d) {
					var maxY = (y(blue_value) < y(red_value)) ? y(blue_value) : y(red_value);
					return maxY;
				})
				.style("stroke-dasharray", ("3, 3"))
				.attr("stroke-width", 1)
				.attr("stroke", "white");

			blueSelectedLine
				.attr("x1", function(d) {
					return x(time) + delta; 
				})
				.attr("x2", function(d) {
					return 0;
				})
				.attr("y1", function(d) {
					return y(blue_value);
				})
				.attr("y2", function(d) {
					return y(blue_value);
				})
				.style("stroke-dasharray", ("3, 3"))
				.attr("stroke-width", 1)
				.attr("stroke", "steelblue");

			redSelectedLine
				.attr("x1", function(d) {
					return x(time) + delta; 
				})
				.attr("x2", function(d) {
					return 0;
				})
				.attr("y1", function(d) {
					return y(red_value);
				})
				.attr("y2", function(d) {
					return y(red_value);
				})
				.style("stroke-dasharray", ("3, 3"))
				.attr("stroke-width", 1)
				.attr("stroke", "red");

			/**
			 * Red Circles
			 */
			redSelectedCircle1
				.attr("cx", function(d) {
					return x(time) + delta;
				 })
				.attr("cy", function(d) {
					return y(red_value);
				})
				.attr("r", 8)
				.attr("fill", "red");

			redSelectedCircle2
				.attr("cx", function(d) {
					return x(time) + delta;
				 })
				.attr("cy", function(d) {
					return y(red_value);
				})
				.attr("r", 6)
				.attr("fill", "white");

			redSelectedCircle3
			.attr("cx", function(d) {
				return x(time) + delta;
			 })
			.attr("cy", function(d) {
				return y(red_value);
			})
			.attr("r", 4)
			.attr("fill", "red");


			/**
			 * Blue Circles
			 */
			blueSelectedCircle1
				.attr("cx", function(d) {
					return x(time) + delta;
				 })
				.attr("cy", function(d) {
					return y(blue_value);
				})
				.attr("r", 8)
				.attr("fill", "steelblue");

			blueSelectedCircle2
				.attr("cx", function(d) {
					return x(time) + delta;
				 })
				.attr("cy", function(d) {
					return y(blue_value);
				})
				.attr("r", 6)
				.attr("fill", "white");

				blueSelectedCircle3
				.attr("cx", function(d) {
					return x(time) + delta;
				 })
				.attr("cy", function(d) {
					return y(blue_value);
				})
				.attr("r", 4)
				.attr("fill", "steelblue");

		});

	}
		

}

module.exports = {
	TimelineView: TimelineView,
	ObjectivesView: ObjectivesView
}
